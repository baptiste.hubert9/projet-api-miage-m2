package org.miage.coursity.View;

public class View{
    
    public static class Utilisateur {}

    public static class getNombreEpisode extends Utilisateur {}

    public static class getEpisodes extends Utilisateur {}

    public static class getCatalogueConnecte extends Utilisateur {}


    public static class EpisodePasPossede extends getEpisodes {}

    public static class Episode extends getEpisodes {}

    public static class EpisodeDeconnecte extends getEpisodes {}

    public static class dejaVu extends Episode {} 
    
    public static class getCatalogueConnecteAll extends EpisodePasPossede {}

    public static class getCatalogueConnecteTri extends Episode {}

    public static class getCatalogueConnectePossession extends getCatalogueConnecteTri {}
}
