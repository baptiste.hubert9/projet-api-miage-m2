
package org.miage.coursity;

import java.security.interfaces.EdECKey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;


@SpringBootApplication
public class Coursity {
	
    @Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI().info(new Info()
		.title("Coursity API")
		.version("1.0")
		.description("Documentation Coursity API Miage"));
	}
		
	public static void main(String[] args) {
		SpringApplication.run(Coursity.class, args);
	}
}