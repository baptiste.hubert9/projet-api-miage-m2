package org.miage.coursity.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;



@EnableWebSecurity
@Configuration
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private UtilisateursDetailsService utilisateurDetailsService;

    @Autowired 
    public CustomWebSecurityConfigurerAdapter() {
    }


    public void loginUtilisateur(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(utilisateurDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable().formLogin().disable()
            .authorizeRequests()
                .antMatchers("/cours/**").permitAll()
                .antMatchers("/monCompte/**").authenticated()
                .and()
            .formLogin()
                .defaultSuccessUrl("/monCompte/", true);
        http.httpBasic();
    }



    @Bean
    public static BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();   
    }

}