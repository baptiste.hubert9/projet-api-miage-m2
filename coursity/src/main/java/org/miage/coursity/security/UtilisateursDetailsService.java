package org.miage.coursity.security;

import org.miage.coursity.boundary.UtilisateurResource;
import org.miage.coursity.entity.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UtilisateursDetailsService implements UserDetailsService {

    @Autowired
    private UtilisateurResource ures;

    @Override
    public UserDetails loadUserByUsername(String mail) {
        Utilisateur utilisateur = ures.findByMail(mail);
        if (utilisateur == null) {
            throw new UsernameNotFoundException(mail);
        }
        return utilisateur;
    }
}