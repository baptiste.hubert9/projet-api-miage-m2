package org.miage.coursity.boundary;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.EntityModel;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonView;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.miage.coursity.entity.CarteBancaire;
import org.miage.coursity.entity.Cours;
import org.miage.coursity.entity.Episode;
import org.miage.coursity.entity.Utilisateur;
import org.miage.coursity.entity.UtilisateurEpisode;
import org.miage.coursity.View.View;

@RestController
@RequestMapping(value = "/monCompte", produces = MediaType.APPLICATION_JSON_VALUE)
public class CompteUtilisateurRepresentation {

    private final CoursResource cres;
    private final EpisodeResource eres;
    private final UtilisateurEpisodesResource ueres;
    private final UtilisateurResource ures;
    private List<Cours> listeCoursUtilisateurPerso;
    private String idUtilisateurConnecte ;

        //Utilisateur util = (Utilisateur)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //String idUtilisateurConnecte = util.getId();

    public CompteUtilisateurRepresentation(CoursResource cres, EpisodeResource eres, UtilisateurEpisodesResource ueres, UtilisateurResource ures) {
        this.cres = cres;
        this.eres = eres;
        this.ueres = ueres;
        this.ures = ures;
    }

    public void loadIdUtilisateur(){
        this.idUtilisateurConnecte = ((Utilisateur)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
    }

    // Identification de l'utilisateur
    @GetMapping(value = "/")
    @JsonView(View.dejaVu.class)
    public ResponseEntity<?> getBasique() {
        Utilisateur util = (Utilisateur)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String infoUtilisateur = util.getNomUtil();
        System.out.println("Vous êtes connecté(e) : Bienvenue " + infoUtilisateur);
        
        Link versCompte = linkTo(methodOn(CompteUtilisateurRepresentation.class).getConnecte()).withRel("CoursCompte");
        Link versCatalogue = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursCatalogue()).withRel("Catalogue");
        Link versGestionCarteBancaire = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCarteBancaire()).withRel("GET - POST - DELETE possible sur la carte bancaire");
         
        String commandePossible = "Possibilité d'aller dans vos cours : " + versCompte + System.lineSeparator()
         + "Possibilité d'aller sur le catalogue : " + versCatalogue + System.lineSeparator() 
         + "Possibilité d'ajouter une carte bancaire" + versGestionCarteBancaire;
        return ResponseEntity.ok(commandePossible);
    }

    // Identification de l'utilisateur
    @GetMapping(value = "/mesCours")
    @JsonView(View.dejaVu.class)
    public ResponseEntity<?> getConnecte() {
        //Récupération des informations sur l'utilisateur
        Utilisateur util = (Utilisateur)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String infoUtilisateur = util.getNomUtil();
        idUtilisateurConnecte = util.getId();
        loadCoursEpisodes();
        if (!verificationListeCoursVide()){
            reloadEpisodePOSTUtilisateur();
            Iterable<Cours> all = listeCoursUtilisateurPerso;
            return ResponseEntity.ok(coursToResourceCompte(all));
        } else {
            return messagePasDeCoursDansLaBase();
        }

        
    }

    //Récupération des cours et des épisodes de l'utilisateur
    public void loadCoursEpisodes(){
        loadIdUtilisateur();
        //Initialisation des achats de l'utilisateurs
        listeCoursUtilisateurPerso = new ArrayList<Cours>();
        String idCoursActuel = "";
        Cours coursUtilisateur = null;
        
        //Récupération de ses liens avec cours et épisodes
        List<UtilisateurEpisode> listeCatalogueUtilisateur = ueres.findByIdUtilisateurOrderByIdLienAsc(idUtilisateurConnecte);
        for (UtilisateurEpisode ue : listeCatalogueUtilisateur){
            //Récupération de l'id de l'épisode dans la liste du catalogue des utilisateurs
            String idEpisodeUtilisateurActuel = ue.getIdEpisode();
            //Récupération de l'épisode dans la table des épisodes grace à l'identifiant récupéré plus haut  
            Episode episodeActuel = eres.findById(idEpisodeUtilisateurActuel).get();
            //Récupération du cours lié à l'épisode actuel 
            Cours coursActuel = cres.findById(episodeActuel.getIdCours()).get();
            //Récupération de l'id du cours associé à l'épisode actuel
            String idCoursGet = coursActuel.getId();
            //String idCoursGet =cres.findById(eres.findById(ue.getIdEpisode()).get().getIdCours()).get().getId();
            //Nous sommes dans une "rupture" pour un cours
            if (idCoursGet.equals(idCoursActuel)){
                //Toujours même cours
                Episode episodeUtilisateur = eres.findById(ue.getIdEpisode()).get();
                episodeUtilisateur.setDejaVu(ue.getDejaRegarde());
                coursUtilisateur.getListeEpisode().add(episodeUtilisateur);  
            } else {
                if (coursUtilisateur != null ){
                    listeCoursUtilisateurPerso.add(coursUtilisateur);
                }
                //Ajout de l'id car on est dans une nouvelle rupture
                idCoursActuel = idCoursGet;
                
                //Récupération des informations du cours
                coursUtilisateur = cres.findById(idCoursGet).get();
                //Récupération de l'épisode -> Si déjà vu, alors on redonene ça en information à l'utlisateur
                Episode episodeUtilisateur = eres.findById(ue.getIdEpisode()).get();
                episodeUtilisateur.setDejaVu(ue.getDejaRegarde());

                //Création de la liste d'épisode pour le cours
                coursUtilisateur.setListeEpisode(new ArrayList<Episode>());
                coursUtilisateur.getListeEpisode().add(episodeUtilisateur);
            }
        }
        listeCoursUtilisateurPerso.add(coursUtilisateur);
        //Quand l'utilisateur va se connecter, on va de base lui attribuer sa liste de cours
    }


    //Ajout dans la table des épisodes d'un utilisateur pour se mettre à jour - Problème de conception, car ce code devrait se trouver dans le code du POST et PUT dans la partie admin
    public void reloadEpisodePOSTUtilisateur(){
        for (Cours cours : this.listeCoursUtilisateurPerso){
            List<Episode> listeEpisodeCours = eres.findByIdCours(cours.getId());
            for (Episode episode : listeEpisodeCours){
                if (ueres.findByIdUtilisateurAndIdEpisode(idUtilisateurConnecte, episode.getId()) == null){
                    UtilisateurEpisode utilisateurEpisode = new UtilisateurEpisode(idUtilisateurConnecte, episode.getId(), "non");
                    ueres.save(utilisateurEpisode);
                }
            }
        }
    }

    
    //Mise à jour des épisodes d'un cours quand ce dernier à été regardé
    public void reloadEpisodesForCours(String idCours){
        for (Cours cours : this.listeCoursUtilisateurPerso){
            if (cours.getId().equals(idCours)){
                for (Episode episode : cours.getListeEpisode()){
                    UtilisateurEpisode episodeRegardee = ueres.findByIdUtilisateurAndIdEpisode(idUtilisateurConnecte, episode.getId());
                    episode.setDejaVu(episodeRegardee.getDejaRegarde());
                }
            }
        }
    }

    // GET one
    @JsonView(View.dejaVu.class)
    @GetMapping(value = "/mesCours/{CoursId}")
    public ResponseEntity<?> getCoursUtilisateur(@PathVariable("CoursId") String idCoursSelec) {
        //Récupération du code
        loadCoursEpisodes();
        if (!verificationListeCoursVide()){
            reloadEpisodePOSTUtilisateur();
            Cours coursRecupere = null;
            for (Cours cours : this.listeCoursUtilisateurPerso){
                if (cours.getId().equals(idCoursSelec)){
                    coursRecupere = cours;
                    for (Episode episode : coursRecupere.getListeEpisode()){
                        Link lienEpisode = linkTo(methodOn(CompteUtilisateurRepresentation.class).getEpisodeUtilisateurCompte(episode.getIdCours(), episode.getId())).withSelfRel();
                        episode.add(lienEpisode);
                    }
                }
            }
            // Si le cours existe, on retourne un résultat, sinon on retourne un code HTTP not found
            if (coursRecupere != null){
                return ResponseEntity.ok(coursToResourceCompte(coursRecupere, false));
            } else {
                return ResponseEntity.notFound().build();      
            }
        } else {
            return messagePasDeCoursDansLaBase();
        }

        
    }

    //Gestion si l'utilisateur n'a pas de cours sur son compte
    public ResponseEntity<?> messagePasDeCoursDansLaBase() {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Vous n'avez aucun cours dans votre compte. Affichage de résultats impossible.");
    }

    //On verifie si la liste des cours de l'utilisateur est null
    public boolean verificationListeCoursVide(){
        boolean listeEstVide = false;
        //Lors de la méthode loadCoursEpisodes(), on ajoute au moins un cours (même null) à la liste des cours de l'utilisateur
        if (listeCoursUtilisateurPerso.get(0) == null){
            listeEstVide = true;
        }
        return listeEstVide;
    }



    // GET un épisode - quand l'utilisateur effectue cette commande, l'épisode passe à déja regardé dans la table Utilisateur_episodes
    @GetMapping(value = "/mesCours/{CoursId}/{EpisodeId}")
    public ResponseEntity<?> getEpisodeUtilisateurCompte(@PathVariable("CoursId") String idCoursSelec, @PathVariable("EpisodeId") String idEpisodeSelec) {
        loadCoursEpisodes();
        if (!verificationListeCoursVide()){
            reloadEpisodesForCours(idCoursSelec);
            Cours coursRecupere = null;
            for (Cours cours : this.listeCoursUtilisateurPerso){
                if (cours.getId().equals(idCoursSelec)){
                    coursRecupere = cours;
                }
            }

            if (coursRecupere != null){
                Episode episodeRecupere = null;
                for (Episode episode : coursRecupere.getListeEpisode()){
                    if (episode.getId().equals(idEpisodeSelec)){
                        episodeRecupere = episode;
                        if (episode.getDejaVu().equals("non")){
                            UtilisateurEpisode episodeRegardee = ueres.findByIdUtilisateurAndIdEpisode(idUtilisateurConnecte, episode.getId());
                            episodeRegardee.setDejaRegarde("oui");
                            ueres.save(episodeRegardee);
                        }
                    }
                }
                if (episodeRecupere != null){
                    return ResponseEntity.ok(episodeToResourceCompte(episodeRecupere, false));
                } else {
                    return ResponseEntity.notFound().build();     
                }
            } else {
                return ResponseEntity.notFound().build();      
            }   
        } else {
            return messagePasDeCoursDansLaBase();
        }
       
        
        
    }

//-----------------------------------------------Affichage des cours quand l'utilisateur est connecté----------------

    // GET all - Affichages des cours avec leur nombre d'épisodes
    @GetMapping(value = "/CatalogueCours")
    @JsonView(View.getCatalogueConnecte.class)
    public ResponseEntity<?> getAllCoursCatalogue() {
        loadCoursEpisodes();
        Iterable<Cours> all = cres.findAll();
        for (Cours cfor: all){
            cfor.setNombreEpisode(eres.findByIdCours(cfor.getId()).size());
            if (verificationListeCoursVide()){
                cfor.setPossede("non");
            } else {
                CoursSetPossede(cfor);
            }
        }
        return ResponseEntity.ok(coursToResourceCatalogue(all));
    }

    // GET all - Affichages des cours ainsi que leurs listes d'épisodes
    @GetMapping(value = "/CatalogueCoursAllEpisodes")
    @JsonView(View.getCatalogueConnecteAll.class)
    public ResponseEntity<?> getAllCoursEpisodeCatalogue() {
        loadCoursEpisodes();
        Iterable<Cours> all = cres.findAll();
        for (Cours cfor: all){
            cfor.setListeEpisode(eres.findByIdCours(cfor.getId()));
            for (Episode efor : cfor.getListeEpisode()){
                efor.add(linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursOneEpisodeCatalogue(cfor.getId(), efor.getId())).withSelfRel());
            }
            if (verificationListeCoursVide()){
                cfor.setPossede("non");
            } else {
                CoursSetPossede(cfor);
            }
        }
        return ResponseEntity.ok(coursToResourceCatalogue(all));
    }

    // GET all - Affichages des cours mais seulement ceux qu'on a pas encore achetés
    @GetMapping(value = "/CatalogueCoursFiltreCompte")
    @JsonView(View.getCatalogueConnecteAll.class)
    public ResponseEntity<?> getAllCoursEpisodeCatalogueFiltre() {
        loadCoursEpisodes();
        if (verificationListeCoursVide()){
            return getAllCoursEpisodeCatalogue();
        } else {
            Iterable<Cours> all = cres.findAll();
            for (Cours cfor: all){
                cfor.setListeEpisode(eres.findByIdCours(cfor.getId()));
            }
            ArrayList<Cours> coursTri = new ArrayList();
            all.forEach(coursFor -> coursTri.add(coursFor));
            Iterable<Cours> retourTri = VerificationCoursAcheteCatalogue(coursTri);
            return ResponseEntity.ok(coursToResourceCatalogue(retourTri));
        }
    }


    // GET one// GET one - Affichage d'un cours en particulier
    @GetMapping(value = "/CatalogueCours/{CoursId}")
    @JsonView(View.getCatalogueConnecteAll.class)
    public ResponseEntity<?> getCoursCatalogue(@PathVariable("CoursId") String idCoursSelec){
            if (!cres.findById(idCoursSelec).isEmpty()){
                Cours cours = cres.findById(idCoursSelec).get();
                cours.setListeEpisode(eres.findByIdCours(cours.getId()));
                CoursSetPossede(cours);
                Iterable<Episode> episodeResource = cours.getListeEpisode();
                return ResponseEntity.ok(episodeToResourceCatalogue(episodeResource));
            } else {
                return ResponseEntity.notFound().build();
            }

        
    }

    @GetMapping(value="/CatalogueCours/{CoursId}/{EpisodeId}")    
    @JsonView(View.getCatalogueConnecteAll.class)
    public ResponseEntity<?> getCoursOneEpisodeCatalogue(@PathVariable("CoursId") String idCoursSelec,@PathVariable("EpisodeId") String idEpisodeSelec) {
        //Vérification que l'id de Cours fourni est correct
        if (!cres.findById(idCoursSelec).isEmpty()){
            if (!eres.findById(idEpisodeSelec).isEmpty()){
                //return ResponseEntity.ok(episodeToResource(eres.findById(idEpisodeSelec).get(), true));
                Episode ep = eres.findById(idEpisodeSelec).get();
                ep.add(linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursOneEpisodeCatalogue(idCoursSelec, idEpisodeSelec)).withSelfRel());
                return new ResponseEntity<>(ep, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build(); 
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


 //------------------- Achat d'un cours par un utilisateur connecté-----------------------   

    // POST - Achat d'un cours
    @PostMapping(value= "/CatalogueCours/{CoursId}")
    @JsonView(View.dejaVu.class)
    public ResponseEntity<?> achatCours(@PathVariable("CoursId") String idCoursSelec) {
        loadIdUtilisateur();
        loadCoursEpisodes();
        //Si l'utilisateur a une carte bancaire chargé alors il peut effectuer l'achat
        Utilisateur utilisateurActuel = ures.findById(idUtilisateurConnecte).get();

        
        //Vérification que l'id du cours rentrée existe
        if (!cres.findById(idCoursSelec).isEmpty()){
            //Vérification que l'utilisateur ne possède pas déjà le cours
            boolean traitementimpossible = false;
            if (!verificationListeCoursVide()){
                for (Cours cours : listeCoursUtilisateurPerso){
                    if (cours.getId().equals(idCoursSelec)){
                        traitementimpossible = true;
                    } 
                } 
            }

            if (traitementimpossible){
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Vous possédez déjà ce cours");
            } else {
                if (utilisateurActuel.getCarteBancaire() != null){
                    Cours cours = cres.findById(idCoursSelec).get();
                    cours.setListeEpisode(eres.findByIdCours(cours.getId()));
                    for (Episode episode : cours.getListeEpisode()){
                        UtilisateurEpisode utilisateurEpisode = new UtilisateurEpisode(idUtilisateurConnecte, episode.getId(), "non");
                        ueres.save(utilisateurEpisode);
                    }
                    loadCoursEpisodes();
                    URI voirCours = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursUtilisateur(cours.getId())).toUri(); 
                    return ResponseEntity.created(voirCours).build();
                }else{
                    Link lienEnregistrementCarteBancaire = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCarteBancaire()).withSelfRel();
                    String body = "Vous n'avez pas de carte bancaire enregistré sur votre compte. Veuillez en enregistrer une à cette addresse (POST) :" +  lienEnregistrementCarteBancaire;
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
                }
            }

            
        } else {
            return ResponseEntity.notFound().build();
        }
        
    }  

//----------------------------------------Fonctions métiers pour gérer le tri du catalogue une fois connecté---------------------------------------
    //Afficher tous les cours que l'utilisateur ne possède pas déjà
    public Iterable<Cours> VerificationCoursAcheteCatalogue(ArrayList<Cours> listeCoursCatalogue){
        loadCoursEpisodes();
        List<Cours> listeCoursDejaPossede = new ArrayList();
        for (Cours coursIterable : listeCoursCatalogue){
            if (VerificationCoursAchetePerso(coursIterable.getId()) == true){
                listeCoursDejaPossede.add(coursIterable);
            }
        }
        for (Cours coursASupprimer : listeCoursDejaPossede){
            listeCoursCatalogue.remove(coursASupprimer);
        }
        Iterable<Cours> iterableTri = listeCoursCatalogue;
        return iterableTri;
    }

    //Vérification qu'un cours est possédé par l'utilisateur
    public Boolean VerificationCoursAchetePerso(String idCours){
        Boolean affichage = false;
        for (Cours cours : this.listeCoursUtilisateurPerso){
            if (cours.getId().equals(idCours)){
                affichage = true;
            }
        }
        return affichage;
    }

    //Méthode pour pouvoir affiché oui ou non à côté de l'attribut possédé pour un cours
    public void CoursSetPossede(Cours cours){
        loadCoursEpisodes();
        Boolean affichage = false;
        for (Cours c : this.listeCoursUtilisateurPerso){
            if (c.getId().equals(cours.getId())){
                affichage = true;
            }
        }
        if (affichage){
            cours.setPossede("oui");
        } else {
            cours.setPossede("non");
        }
    }




// Partie Carte Bancaire de l'utilisateur

    
    // GET - Récupération de la carte bancaire de l'utilisateur - Erreur 404 si pas de carte bancaire enregistrée, sinon code 200
    @GetMapping(value = "/maCarteBancaire")
    @JsonView(View.dejaVu.class)
    public ResponseEntity<?> getCarteBancaire() {
        loadIdUtilisateur();
        Utilisateur utilisateurActuel = ures.findById(idUtilisateurConnecte).get();
        if (utilisateurActuel.getCarteBancaire() != null){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }   

    // POST - Ajout d'une carte bancaire dans le compte de l'utilisateur
    @PostMapping(value="/maCarteBancaire")
    @JsonView(View.dejaVu.class)
    public ResponseEntity<?> postCarteBancaire(@RequestBody @Valid CarteBancaire cb) {
        loadIdUtilisateur();
        Utilisateur utilisateurActuelMaj = ures.findById(idUtilisateurConnecte).get();
        utilisateurActuelMaj.setCarteBancaire(cb.getNumeroCarteBancaire());
        ures.save(utilisateurActuelMaj);
        return ResponseEntity.ok().build();
    }  

    // DELETE - Suppression d'un épisode. Suppression possible seulement s'il reste au moins un episode après suppression
    @DeleteMapping(value = "/maCarteBancaire")
    @Transactional
    public ResponseEntity<?> deleteCarteBancaire() {
        loadIdUtilisateur();
        Utilisateur utilisateurActuel = ures.findById(idUtilisateurConnecte).get();
        if (utilisateurActuel.getCarteBancaire() == null){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Vous n'avez pas de carte bancaire à supprimer");
        } else {
            utilisateurActuel.setCarteBancaire(null);
            return ResponseEntity.noContent().build();
        }
    }


  //--------------Affichage selon les normes HATEOAS -----------
//-----------Affichage des cours
    private CollectionModel<EntityModel<Cours>> coursToResourceCompte(Iterable<Cours> cours) {
        Link selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getConnecte()).withSelfRel();
        Link versCatalogue = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursCatalogue()).withRel("Catalogue");
        Link versGestionCarteBancaire = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCarteBancaire()).withRel("GET - POST - DELETE possible sur la carte bancaire");
        List<EntityModel<Cours>> coursResources = new ArrayList();
        cours.forEach(coursFor -> coursResources.add(coursToResourceCompte(coursFor, false)));
        return CollectionModel.of(coursResources, selfLink, versCatalogue,versGestionCarteBancaire);
    }

    private EntityModel<Cours> coursToResourceCompte(Cours cours, Boolean collection) {
        var selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursUtilisateur(cours.getId())).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getConnecte())
                .withRel("collection");
            return EntityModel.of(cours, selfLink, collectionLink);
        } else {
            return EntityModel.of(cours, selfLink);
        }
    }

    private CollectionModel<EntityModel<Cours>> coursToResourceCatalogue(Iterable<Cours> cours) {
        Link selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursCatalogue()).withSelfRel();
        Link versCompte = linkTo(methodOn(CompteUtilisateurRepresentation.class).getConnecte()).withRel("CoursCompte");
        List<EntityModel<Cours>> coursResources = new ArrayList();
        cours.forEach(coursFor -> coursResources.add(coursToResourceCatalogue(coursFor, false)));
        return CollectionModel.of(coursResources, selfLink, versCompte);
    }

    private EntityModel<Cours> coursToResourceCatalogue(Cours cours, Boolean collection) {
        var selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursCatalogue(cours.getId())).withSelfRel();
        
        var achatCours = linkTo(methodOn(CompteUtilisateurRepresentation.class).achatCours(cours.getId())).withRel("Achat du cours - POST");
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursCatalogue()).withSelfRel()  
                .withRel("collection");
            return EntityModel.of(cours, selfLink, achatCours, collectionLink);
        } else {
            return EntityModel.of(cours, selfLink, achatCours);
        }
    }

//-----------Affichage des épisodes

    private EntityModel<Episode> episodeToResourceCompte(Episode episode, Boolean collection) {
        var selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getEpisodeUtilisateurCompte(episode.getIdCours(), episode.getId())).withSelfRel();
        return EntityModel.of(episode, selfLink);
    }


    private CollectionModel<EntityModel<Episode>> episodeToResourceCatalogue(Iterable<Episode> episode) {
        Link selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursEpisodeCatalogue()).withSelfRel();
        List<EntityModel<Episode>> episodeResources = new ArrayList();
        episode.forEach(episodeFor -> episodeResources.add(episodeToResourceCatalogue(episodeFor, false)));
        return CollectionModel.of(episodeResources, selfLink);
    }

    private EntityModel<Episode> episodeToResourceCatalogue(Episode episode, Boolean collection) {
        var selfLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getCoursOneEpisodeCatalogue(episode.getIdCours(), episode.getId())).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CompteUtilisateurRepresentation.class).getAllCoursEpisodeCatalogue())
                .withRel("collection");
            return EntityModel.of(episode, selfLink, collectionLink);
        } else {
            return EntityModel.of(episode, selfLink);
        }
    }
}