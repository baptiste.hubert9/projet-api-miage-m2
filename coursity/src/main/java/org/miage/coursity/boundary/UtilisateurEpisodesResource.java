package org.miage.coursity.boundary;

import java.util.List;

import org.miage.coursity.entity.UtilisateurEpisode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurEpisodesResource extends JpaRepository<UtilisateurEpisode, String> {

    List<UtilisateurEpisode> findByIdUtilisateurOrderByIdLienAsc(String idUtilisateur);

    UtilisateurEpisode findByIdUtilisateurAndIdEpisode(String idUtilisateur, String idEpisode);
}