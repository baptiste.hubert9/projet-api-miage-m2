package org.miage.coursity.boundary;


import org.miage.coursity.entity.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurResource extends JpaRepository<Utilisateur, String> {

    Utilisateur findByMail(String mail);
}