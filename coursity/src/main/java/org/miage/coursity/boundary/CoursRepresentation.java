package org.miage.coursity.boundary;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonView;

import org.miage.coursity.entity.Cours;
import org.miage.coursity.entity.Episode;
import org.miage.coursity.entity.Utilisateur;
import org.miage.coursity.security.CustomWebSecurityConfigurerAdapter;
import org.miage.coursity.View.View;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;

@RestController
@RequestMapping(value="/cours", produces=MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Cours.class)
public class CoursRepresentation {
    
    private final CoursResource cres;
    private final EpisodeResource eres;
    private final UtilisateurResource ures;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    public CoursRepresentation(CoursResource cres, EpisodeResource eres, UtilisateurResource ures) {
        this.cres = cres;
        this.eres=eres;
        this.ures = ures;
    }

    // GET all - Affichages des cours ainsi que leurs nombre d'épisode
    @GetMapping("/")
    @JsonView(View.getNombreEpisode.class)
    public ResponseEntity<?> getAllCours() {
        Iterable<Cours> all = cres.findAll();
        for (Cours cfor: all){
            cfor.setNombreEpisode(eres.findByIdCours(cfor.getId()).size());
        }
        return ResponseEntity.ok(coursToResource(all));
    }

    // POST - Création d'un compte utilisateur
    @PostMapping(value = "/Inscription")
    @Transactional
    public ResponseEntity<?> addUtilisateur(@RequestBody @Valid Utilisateur utilisateur) {
        if (ures.findByMail(utilisateur.getMail()) == null){
            Utilisateur util = new Utilisateur(UUID.randomUUID().toString(), utilisateur.getMail(), CustomWebSecurityConfigurerAdapter.passwordEncoder().encode(utilisateur.getPassword()) ,utilisateur.getNom(), utilisateur.getPrenom(), utilisateur.getCarteBancaire());
            ures.save(util);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Un utilisateur possède déjà l'email que vous utilisez.");
        }
    }

    // GET - Affichage de tous les cours avec tous les épisodes dans la base
    @GetMapping("/allCours")
    @JsonView(View.EpisodeDeconnecte.class)
    public ResponseEntity<?> getAllCoursEpisodes() {
        Iterable<Cours> all = cres.findAll();
        for (Cours cfor: all){
            cfor.setListeEpisode(eres.findByIdCours(cfor.getId()));
        }
        return ResponseEntity.ok(coursToResource(all));
    }
    
    // GET un cours et tous ses épisodes
    @GetMapping(value="/{CoursId}")
    @JsonView(View.EpisodeDeconnecte.class)
    public ResponseEntity<?> getCours(@PathVariable("CoursId") String idCoursSelec) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            Cours cours = cres.findById(idCoursSelec).get();
            List<Episode> listeEpisodes = eres.findByIdCours(cours.getId());
            for (Episode ep : listeEpisodes){
                var lienEpisode = linkTo(methodOn(CoursRepresentation.class).getCoursOneEpisode(ep.getIdCours(), ep.getId())).withSelfRel();
                ep.add(lienEpisode);
            }
            cours.setListeEpisode(listeEpisodes);
            
            return ResponseEntity.ok(coursToResource(cours, true));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // GET one - Affichage d'un episode d'un cours en particulier
    @GetMapping(value="/{CoursId}/{EpisodeId}")
    @JsonView(View.EpisodeDeconnecte.class)
    public ResponseEntity<?> getCoursOneEpisode(@PathVariable("CoursId") String idCoursSelec,@PathVariable("EpisodeId") String idEpisodeSelec) {
        //Vérification que l'id de Cours fourni est correct
        if (!cres.findById(idCoursSelec).isEmpty()){
            if (!eres.findById(idEpisodeSelec).isEmpty()){
                Episode ep = eres.findById(idEpisodeSelec).get();
                ep.add(linkTo(methodOn(CoursRepresentation.class).getCoursOneEpisode(idCoursSelec, idEpisodeSelec)).withSelfRel());
                return new ResponseEntity<>(ep, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build(); 
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private CollectionModel<EntityModel<Cours>> coursToResource(Iterable<Cours> IterableCours) {
        Link selfLink = linkTo(methodOn(CoursRepresentation.class).getAllCours()).withSelfRel();
        Link inscription = linkTo(methodOn(CoursRepresentation.class).addUtilisateur(null)).withRel("Inscription en tant qu'utilisateur ici - POST");
        List<EntityModel<Cours>> CoursResources = new ArrayList();
        IterableCours.forEach(Cours -> CoursResources.add(coursToResource(Cours, false)));
        return CollectionModel.of(CoursResources, selfLink, inscription);
    }

    private EntityModel<Cours> coursToResource(Cours Cours, Boolean collection) {
        var selfLink = linkTo(CoursRepresentation.class).slash(Cours.getId()).withSelfRel();
        
        var possibiliteAchat = linkTo(methodOn(CompteUtilisateurRepresentation.class).achatCours(Cours.getId())).withRel("Achat possible si connecté à cette adresse - POST");
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CoursRepresentation.class).getAllCours())
                    .withRel("collection");
            return EntityModel.of(Cours, selfLink, possibiliteAchat, collectionLink);
        } else {
            return EntityModel.of(Cours, selfLink, possibiliteAchat);
        }
    }

    private CollectionModel<EntityModel<Episode>> episodeToResource(Iterable<Episode> episode) {
        Link selfLink = linkTo(methodOn(CoursRepresentation.class).getAllCoursEpisodes()).withSelfRel();
        
        List<EntityModel<Episode>> episodeResources = new ArrayList();
        episode.forEach(episodeFor -> episodeResources.add(episodeToResource(episodeFor, false)));
        return CollectionModel.of(episodeResources, selfLink);
    }

    private EntityModel<Episode> episodeToResource(Episode episode, Boolean collection) {
        var selfLink = linkTo(methodOn(CoursRepresentation.class).getCoursOneEpisode(episode.getIdCours(), episode.getId())).withSelfRel();
        System.out.println("Lien attendu pour episode" + episode.getId() + " est " + selfLink);
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CoursRepresentation.class).getAllCoursEpisodes())
                    .withRel("collection");
            return EntityModel.of(episode, selfLink, collectionLink);
        } else {
            return EntityModel.of(episode, selfLink);
        }
    }
}
