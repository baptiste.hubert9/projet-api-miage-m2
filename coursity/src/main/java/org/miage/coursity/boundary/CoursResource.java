package org.miage.coursity.boundary;

import org.miage.coursity.entity.Cours;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursResource extends JpaRepository<Cours, String> {
}