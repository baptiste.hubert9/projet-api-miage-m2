package org.miage.coursity.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonView;

import org.miage.coursity.View.View;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.NonNull;


@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Cours implements Serializable {

    private static final long serialVersionUID = 8765432234567L;
    @Id
    @NonNull
    private String id;

    @NonNull
    @JsonView(View.Utilisateur.class)
    private String theme;

    @NonNull
    @JsonView(View.Utilisateur.class)
    private float prixEnEuros;

    @Transient
    @JsonView({View.getNombreEpisode.class, View.getCatalogueConnecte.class})
    private long nombreEpisode;

    @Transient
    @JsonView(View.getEpisodes.class)
    private List<Episode> listeEpisode;

    @Transient
    @JsonView({View.getCatalogueConnecte.class, View.getCatalogueConnecteAll.class})
    private String possede;

}