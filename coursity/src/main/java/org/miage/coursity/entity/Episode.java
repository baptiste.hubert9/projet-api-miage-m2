package org.miage.coursity.entity;

import javax.persistence.Transient;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.miage.coursity.View.View;
import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.RequiredArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Episode extends RepresentationModel<Episode> implements Serializable{

    private static final long serialVersionUID = 8765432234567L;

    @Id
    @NonNull
    private String id;

    @JsonIgnore
    @NonNull
    private String idCours;

    @NonNull
    @JsonView({View.Episode.class,View.EpisodeDeconnecte.class, View.EpisodePasPossede.class, View.getCatalogueConnecte.class})
    private String nom;

    @NonNull
    @JsonView({View.Episode.class, View.getCatalogueConnectePossession.class})
    private String source;

    @NonNull
    @JsonView({View.Episode.class,View.EpisodeDeconnecte.class, View.EpisodePasPossede.class, View.getCatalogueConnecte.class})
    private int tempsEnMinutes;

    @Transient
    @JsonView(View.dejaVu.class)
    private String dejaVu;
        
}