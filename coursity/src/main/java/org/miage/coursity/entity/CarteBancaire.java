package org.miage.coursity.entity;

import org.hibernate.validator.constraints.CreditCardNumber;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarteBancaire{

    @CreditCardNumber(message="Le numéro de carte bancaire est incorrect")
    private String numeroCarteBancaire;
    
}
