DROP TABLE IF EXISTS utilisateur;
DROP TABLE IF EXISTS utilisateur_cours;
DROP TABLE IF EXISTS utilisateur_episodes;
DROP TABLE IF EXISTS cours;
DROP TABLE IF EXISTS episode;


CREATE TABLE utilisateur (
  id VARCHAR(36) PRIMARY KEY,
  mail VARCHAR(250) UNIQUE NOT NULL,
  mot_de_passe VARCHAR(60),
  nom VARCHAR(250) NOT NULL,
  prenom VARCHAR(250) NOT NULL,
  carte_bancaire VARCHAR(16)
);

CREATE TABLE utilisateur_episode (
  id_lien SERIAL PRIMARY KEY,
  id_utilisateur VARCHAR(36),
  id_episode VARCHAR(36),
  deja_regarde VARCHAR(3) NOT NULL
);

CREATE TABLE IF NOT EXISTS cours(  
  id VARCHAR(36) PRIMARY KEY,
  theme VARCHAR(250) NOT NULL,
  prix_en_euros INT NOT NULL
);

CREATE TABLE IF NOT EXISTS episode(
  id VARCHAR(36) PRIMARY KEY,
  id_cours VARCHAR(36),
  nom VARCHAR(50) NOT NULL,
  source VARCHAR(250) NOT NULL,
  temps_en_minutes INT NOT NULL
);



/*Partie Foreign Key - Tant qu'il n'y a pas de solution avec JPA, j'utilise cette technique*/
ALTER TABLE episode ADD FOREIGN KEY (id_cours) REFERENCES cours(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE utilisateur_episode ADD FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id) ON DELETE CASCADE ON UPDATE CASCADE;;

ALTER TABLE utilisateur_episode ADD FOREIGN KEY (id_episode) REFERENCES episode(id) ON DELETE CASCADE ON UPDATE CASCADE;;

/*Insertion du jeu de données*/

INSERT INTO cours(id,theme,prix_en_euros) VALUES ('13ec7ab2-67a1-11eb-ae93-0242ac130002','POSTGRESQL', 50);

INSERT INTO episode(id,id_cours,nom,source,temps_en_minutes) VALUES ('49b5c5cc-67a1-11eb-ae93-0242ac130002','13ec7ab2-67a1-11eb-ae93-0242ac130002','Installation de postgresql','test.com/video',50);
INSERT INTO episode(id,id_cours,nom,source,temps_en_minutes) VALUES ('508be28c-67a1-11eb-ae93-0242ac130002','13ec7ab2-67a1-11eb-ae93-0242ac130002','Création de notre première base','test.com/video2',10);

/*mot de passe : mdp1*/
INSERT INTO utilisateur (id,mail,mot_de_passe,nom,prenom) VALUES ('418b01e6-67a1-11eb-ae93-0242ac130002','b@b.fr','$2y$12$VhlaW4EmEwmzH/3WMQv.DuSQo8Ec0wJSVwhe5sOs1Z7h/V1yj.r9O','Parker','Bonnie');

INSERT INTO utilisateur_episode (id_utilisateur,id_episode,deja_regarde) VALUES ('418b01e6-67a1-11eb-ae93-0242ac130002','49b5c5cc-67a1-11eb-ae93-0242ac130002','non');
INSERT INTO utilisateur_episode (id_utilisateur,id_episode,deja_regarde) VALUES ('418b01e6-67a1-11eb-ae93-0242ac130002','508be28c-67a1-11eb-ae93-0242ac130002','non');


/*Mot de passe : mdp2 */
INSERT INTO utilisateur (id,mail,mot_de_passe,nom,prenom) VALUES ('10392528-fd39-4499-95d8-42f2b0503a9b','a@a.fr','$2y$12$cQpC6qu5COSRcIbusssXW.nlMlYnq30xoSRRvzHyyJOqKUhqxmgP.','Barrow','Clyde');

INSERT INTO cours(id,theme,prix_en_euros) VALUES ('10392528-fd39-4499-95d8-42f2b0503a9b','Kubernetes', 50);

INSERT INTO episode(id,id_cours,nom,source,temps_en_minutes) VALUES ('d9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2b','10392528-fd39-4499-95d8-42f2b0503a9b','Introduction','01Net.com',50);
INSERT INTO episode(id,id_cours,nom,source,temps_en_minutes) VALUES ('d9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2c','10392528-fd39-4499-95d8-42f2b0503a9b','Creation d''un projet','01Net.com',50);
INSERT INTO episode(id,id_cours,nom,source,temps_en_minutes) VALUES ('d9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2d','10392528-fd39-4499-95d8-42f2b0503a9b','Mise en place des tests','01Net.com',50);

INSERT INTO utilisateur_episode (id_utilisateur,id_episode,deja_regarde) VALUES ('10392528-fd39-4499-95d8-42f2b0503a9b', 'd9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2b','non');
INSERT INTO utilisateur_episode (id_utilisateur,id_episode,deja_regarde) VALUES ('10392528-fd39-4499-95d8-42f2b0503a9b', 'd9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2c','non');
INSERT INTO utilisateur_episode (id_utilisateur,id_episode,deja_regarde) VALUES ('10392528-fd39-4499-95d8-42f2b0503a9b', 'd9d49d93-59a1-4a73-a2f8-d9ab9a2a9d2d','non');
