package org.miage.admincoursity.boundary;

import org.miage.admincoursity.entity.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurResource extends JpaRepository<Utilisateur, String> {

    Utilisateur findByMail(String mail);
}