package org.miage.admincoursity.boundary;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.miage.admincoursity.entity.Cours;
import org.miage.admincoursity.entity.CoursEpisodes;
import org.miage.admincoursity.entity.Episode;
import org.miage.admincoursity.errors.ApiError;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;


@RestController
@RequestMapping(value="/cours", produces=MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Cours.class)
public class CoursEpisodeRepresentation {

    private final CoursResource cres;
    private final EpisodeResource eres;

    public CoursEpisodeRepresentation(CoursResource cres, EpisodeResource eres) {
        this.cres = cres;
        this.eres = eres;
    }

//--------------Récupération dans la base -------------------------- 

    
    // GET - Affichage de tous les cours dans la base
    @GetMapping("/")
    public ResponseEntity<?> getAllCours() {
        Iterable<Cours> all = cres.findAll();
        for (Cours cfor: all){
            cfor.setListeEpisode(eres.findByIdCours(cfor.getId()));
            for (Episode ep : cfor.getListeEpisode()){
                var lienEpisode = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(ep.getIdCours(), ep.getId())).withSelfRel();
                ep.add(lienEpisode);
            }
        }
        return ResponseEntity.ok(coursToResource(all));
    }

    // GET one - Affichage d'un cours en particulier
    @GetMapping(value="/{CoursId}")
    public ResponseEntity<?> getCours(@PathVariable("CoursId") String idCoursSelec) {
        Cours cours = cres.findById(idCoursSelec).get();
        List<Episode> listeEpisodes = eres.findByIdCours(cours.getId());
        for (Episode ep : listeEpisodes){
            var lienEpisode = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(ep.getIdCours(), ep.getId())).withSelfRel();
            ep.add(lienEpisode);
        }
        cours.setListeEpisode(listeEpisodes);
        return ResponseEntity.ok(coursToResource(cours, true));
    }

    // GET all after one - Affichage de tous les épisodes d'un cours
    @GetMapping(value="/{CoursId}/episodes")
    public ResponseEntity<?> getCoursAllEpisodes(@PathVariable("CoursId") String idCoursSelec) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            Cours cours = cres.findById(idCoursSelec).get();
            cours.setListeEpisode(eres.findByIdCours(cours.getId()));
            Iterable<Episode> episodeResource = cours.getListeEpisode();
            return ResponseEntity.ok(episodeToResource(episodeResource));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // GET one - Affichage d'un episode d'un cours en particulier
    @GetMapping(value="/{CoursId}/{EpisodeId}")
    public ResponseEntity<?> getCoursOneEpisode(@PathVariable("CoursId") String idCoursSelec,@PathVariable("EpisodeId") String idEpisodeSelec) {
        //Vérification que l'id de Cours fourni est correct
        if (!cres.findById(idCoursSelec).isEmpty()){
            if (!eres.findById(idEpisodeSelec).isEmpty()){
                //return ResponseEntity.ok(episodeToResource(eres.findById(idEpisodeSelec).get(), true));
                Episode ep = eres.findById(idEpisodeSelec).get();
                ep.add(linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(idCoursSelec, idEpisodeSelec)).withSelfRel());
                ep.add(linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(idCoursSelec, idEpisodeSelec)).withRel("DELETE, POST & PUT possible"));
                return new ResponseEntity<>(ep, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build(); 
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    

//---------------- Modifications dans la base -------------------------

    // POST - Ajout d'un cours
    @PostMapping(value="/")
    @Transactional
    public ResponseEntity<?> saveCours(@RequestBody @Valid CoursEpisodes coursEpisodes) {
        Cours coursAdd = new Cours(UUID.randomUUID().toString(), coursEpisodes.getCours().getTheme(), coursEpisodes.getCours().getPrixEnEuros());
        Cours saved = cres.save(coursAdd);
        if (!(coursEpisodes.getListeEpisode().size() == 0)){
            for (Episode e : coursEpisodes.getListeEpisode()){
                Episode episodeAdd = new Episode(UUID.randomUUID().toString(), coursAdd.getId(), e.getNom(), e.getSource(), e.getTempsEnMinutes());
                eres.save(episodeAdd);
            }
            linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursAllEpisodes(coursAdd.getId())).withSelfRel();
            URI location = linkTo(CoursEpisodeRepresentation.class).slash(saved.getId()).toUri();
            return ResponseEntity.created(location).build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // POST - Ajout d'épisodes pour un cours
    @PostMapping(value="/{CoursId}/episodes")
    @Transactional
    public ResponseEntity<?> saveEpisodeUnCours(@PathVariable("CoursId") String idCoursSelec, @RequestBody @Valid Episode episode) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            Episode episodeAdd = new Episode(UUID.randomUUID().toString(), idCoursSelec, episode.getNom(), episode.getSource(), episode.getTempsEnMinutes());
            Episode saved = eres.save(episodeAdd);

            URI location = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(idCoursSelec, episodeAdd.getId())).toUri();
            return ResponseEntity.created(location).build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    // PUT - Modification d'un cours - Si n'existe pas, création d'un épisode que l'admin devra modifier
    @PutMapping(value="/{CoursId}")
    @Transactional
    public ResponseEntity<?> updateCours(@RequestBody Cours cours, @PathVariable("CoursId") String idCoursSelec) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            cours.setId(idCoursSelec);
            Cours coursAdd = cres.save(cours);
            URI location = linkTo(CoursEpisodeRepresentation.class).slash(coursAdd.getId()).toUri();
            return ResponseEntity.ok(location);
           
       } else {
            //Création du cours mais on y met aussi un épisode que l'utilisateur devra modifier après.
                Cours coursCreate = new Cours(idCoursSelec, cours.getTheme(), cours.getPrixEnEuros());
                cres.save(coursCreate);
                Episode episodeAdd = new Episode(UUID.randomUUID().toString(), idCoursSelec, "TODO", "TODO", 1);
                eres.save(episodeAdd);
                URI location = linkTo(CoursEpisodeRepresentation.class).slash(coursCreate.getId()).toUri();
                return ResponseEntity.created(location).build();
       }
          
    }


    @PutMapping(value = "/{CoursId}/{EpisodeId}")
    @Transactional
    public ResponseEntity<?> updateCoursEpisodes(@RequestBody Episode episode, @PathVariable("CoursId") String idCoursSelec,@PathVariable("EpisodeId") String idEpisodeSelec) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            if (!eres.findById(idEpisodeSelec).isEmpty()){
                episode.setId(idEpisodeSelec);
                episode.setIdCours(idCoursSelec);
                eres.save(episode); 
                return ResponseEntity.ok().build();
            } else {
                Episode episodeAdd = new Episode(idEpisodeSelec, idCoursSelec, episode.getNom(), episode.getSource(), episode.getTempsEnMinutes());
                eres.save(episodeAdd);
                
                URI location = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursAllEpisodes(idEpisodeSelec)).toUri();
                return ResponseEntity.created(location).build();
                
            }
            
        } else {
            return ResponseEntity.notFound().build();
        }  
    }



    // DELETE - Suppression d'un cours. Suppression possible seulement s'il reste au moins un cours après suppression
    @DeleteMapping(value = "/{CoursId}")
    @Transactional
    public ResponseEntity<?> deleteCours(@PathVariable("CoursId") String idCoursSelec) {
        List<Cours> listeDesCoursTable = cres.findAll();
        Optional<Cours> Cours = cres.findById(idCoursSelec);
        if (listeDesCoursTable.size() >=2){
            if (Cours.isPresent()) {
                cres.delete(Cours.get());
            }
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Il ne reste qu'un cours. Impossible de le supprimer. Utiliser un PUT pour le modifier à la place");
        }
        
    }

    // DELETE - Suppression d'un épisode. Suppression possible seulement s'il reste au moins un episode après suppression
    @DeleteMapping(value = "/{CoursId}/{EpisodeId}")
    @Transactional
    public ResponseEntity<?> deleteEpisode(@PathVariable("CoursId") String idCoursSelec,@PathVariable("EpisodeId") String idEpisodeSelec) {
        if (!cres.findById(idCoursSelec).isEmpty()){
            if (eres.findByIdCours(idCoursSelec).size() >= 2 ){
                if (!eres.findById(idEpisodeSelec).isEmpty()){
                    eres.delete(eres.findById(idEpisodeSelec).get());
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.notFound().build();     
                }
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Il ne reste qu'un épisode pour ce cours. Impossible de le supprimer. Utiliser un PUT pour le modifier à la place");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


//------------- Gestion des erreurs dans les POST (body qui ne respecte pas les contraintes émises dans les classes du package Entity----------------------------------
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    
    @ExceptionHandler(TransactionSystemException.class)
    protected ResponseEntity<Object> handlePersistenceException(final Exception ex, final WebRequest request) {
        Throwable cause = ((TransactionSystemException) ex).getRootCause();
        if (cause instanceof ConstraintViolationException) {        

            ConstraintViolationException consEx= (ConstraintViolationException) cause;
            final List<String> errors = new ArrayList<String>();
            for (final ConstraintViolation<?> violation : consEx.getConstraintViolations()) {
                errors.add(violation.getPropertyPath() + ": " + violation.getMessage());
            }
            final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, consEx.getLocalizedMessage(), errors);
            return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
        } else {
            final ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), "error occurred");
            return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
        }
    }

//--------------Affichage selon les normes HATEOAS -----------
//-----------Affichage des cours
    private CollectionModel<EntityModel<Cours>> coursToResource(Iterable<Cours> cours) {
        Link selfLink = linkTo(methodOn(CoursEpisodeRepresentation.class).getAllCours()).withSelfRel();
        Link uriPostCours = linkTo(methodOn(CoursEpisodeRepresentation.class).saveCours(null)).withRel("Ajouter un cours et des épisodes");
        List<EntityModel<Cours>> coursResources = new ArrayList();
        cours.forEach(coursFor -> coursResources.add(coursToResource(coursFor, false)));
        return CollectionModel.of(coursResources, selfLink, uriPostCours);
    }

    private EntityModel<Cours> coursToResource(Cours cours, Boolean collection) {
        var selfLink = linkTo(CoursEpisodeRepresentation.class).slash(cours.getId()).withSelfRel();
        Link ajoutCours = linkTo(methodOn(CoursEpisodeRepresentation.class).getAllCours()).withRel("POST - Ajouter un cours");
        Link versCoursAllEpisodes = linkTo(methodOn(CoursEpisodeRepresentation.class).getCours(cours.getId())).withRel("Voir le cours et tous ses épisodes");
        Link versEpisodesUnCours = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursAllEpisodes(cours.getId())).withRel("Voir seulement les épisodes du cours");
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CoursEpisodeRepresentation.class).getAllCours())
                    .withRel("collection");
            return EntityModel.of(cours, selfLink, ajoutCours, versCoursAllEpisodes, versEpisodesUnCours, collectionLink);
        } else {
            return EntityModel.of(cours, selfLink, ajoutCours, versCoursAllEpisodes, versEpisodesUnCours);
        }
    }

//-----------Affichage des épisodes

    private CollectionModel<EntityModel<Episode>> episodeToResource(Iterable<Episode> episode) {
        Link selfLink = linkTo(methodOn(CoursEpisodeRepresentation.class).getAllCours()).withSelfRel();
        List<EntityModel<Episode>> episodeResources = new ArrayList();
        episode.forEach(episodeFor -> episodeResources.add(episodeToResource(episodeFor, false)));
        return CollectionModel.of(episodeResources, selfLink);
    }

    private EntityModel<Episode> episodeToResource(Episode episode, Boolean collection) {
        //var selfLink = linkTo(CoursEpisodeRepresentation.class).slash(episode.getId()).withSelfRel();
        var selfLink = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursOneEpisode(episode.getIdCours(), episode.getId())).withSelfRel();
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(CoursEpisodeRepresentation.class).getAllCours())
                    .withRel("collection");
            return EntityModel.of(episode, selfLink, collectionLink);
        } else {
            return EntityModel.of(episode, selfLink);
        }
    }







    //Méthode fantôme - non finie

    /*
    // PUT - Modification d'un cours et de ses épisodes
    @PutMapping(value = "/{CoursId}")
    @Transactional
    public ResponseEntity<?> updateCoursEpisodes(@RequestBody CoursEpisodes coursEpisodes, @PathVariable("CoursId") String idCoursSelec) {
           return cres.findById(idCoursSelec)
                .map(coursMap -> {
                    coursMap = new Cours(idCoursSelec, coursEpisodes.getCours().getTheme(), coursEpisodes.getCours().getPrixEnEuros());
                    Cours saved = cres.save(coursMap);
                    for (Episode ep : coursEpisodes.getListeEpisode()){
                            Episode episodeAdd = new Episode(UUID.randomUUID().toString(), idCoursSelec, ep.getNom(), ep.getSource(), ep.getTempsEnMinutes());
                            eres.save(episodeAdd);
                    }
                    URI location = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursAllEpisodes(idCoursSelec)).toUri();
                    return ResponseEntity.created(location).build();
                })
                .orElseGet(() -> {
                    coursEpisodes.getCours().setId(idCoursSelec);
                  cres.save(coursEpisodes.getCours());
                  for (Episode ep : coursEpisodes.getListeEpisode()){
                    //Si l'episode existe pour le cours alors on le modifie, sinon on le crée
                    if (eres.findById(ep.getId()).isEmpty()){
                        Episode episodeAdd = new Episode(UUID.randomUUID().toString(), idCoursSelec, ep.getNom(), ep.getSource(), ep.getTempsEnMinutes());
                        eres.save(episodeAdd);
                    } else {
                        ep.setId(UUID.randomUUID().toString());
                        eres.save(ep);
                    }
                  }
                  URI location = linkTo(methodOn(CoursEpisodeRepresentation.class).getCoursAllEpisodes(idCoursSelec)).toUri();
                  return ResponseEntity.ok().build();
                });
          
    }*/


}
