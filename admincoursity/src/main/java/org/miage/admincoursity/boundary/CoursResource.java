package org.miage.admincoursity.boundary;

import org.miage.admincoursity.entity.Cours;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoursResource extends JpaRepository<Cours, String> {
    
}