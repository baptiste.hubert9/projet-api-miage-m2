package org.miage.admincoursity.boundary;

import java.util.List;
import org.miage.admincoursity.entity.Episode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EpisodeResource extends JpaRepository<Episode, String> {

    List<Episode> findByIdCours(String id);
}