package org.miage.admincoursity.boundary;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonView;

import org.miage.admincoursity.entity.Utilisateur;
import org.miage.admincoursity.security.CustomWebSecurityConfigurerAdapter;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;


@RestController
@RequestMapping(value="/utilisateurs", produces=MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Utilisateur.class)
public class UtilisateurRepresentation {
    
    private final UtilisateurResource ures;

    public UtilisateurRepresentation(UtilisateurResource ures) {
        this.ures = ures;
    }

    // GET all
    @GetMapping("/")
    public ResponseEntity<?> getAllUtilisateurs() {
        Iterable<Utilisateur> all = ures.findAll();
        return ResponseEntity.ok(utilisateurToResource(all));
    }

    // GET one
    @GetMapping(value="/{utilisateurId}")
    public ResponseEntity<?> getUtilisateur(@PathVariable("utilisateurId") String id) {
        return Optional.ofNullable(ures.findById(id)).filter(Optional::isPresent)
        .map(i -> ResponseEntity.ok(utilisateurToResource(i.get(), true)))
        .orElse(ResponseEntity.notFound().build());
    }

    // DELETE
    @DeleteMapping(value = "/{utilisateurId}")
    @Transactional
    public ResponseEntity<?> deleteUtilisateur(@PathVariable("utilisateurId") String utilisateurId) {
        Optional<Utilisateur> utilisateur = ures.findById(utilisateurId);
            if (utilisateur.isPresent()) {
                ures.delete(utilisateur.get());
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.notFound().build();
            }
        
    }

    // POST
    @PostMapping
    @Transactional
    public ResponseEntity<?> addUtilisateur(@RequestBody @Valid Utilisateur utilisateur) {
        if (ures.findByMail(utilisateur.getMail()) == null){
            Utilisateur util = new Utilisateur(UUID.randomUUID().toString(), utilisateur.getMail(), CustomWebSecurityConfigurerAdapter.passwordEncoder().encode(utilisateur.getPassword()), utilisateur.getNom(), utilisateur.getPrenom(), null);
            Utilisateur saved = ures.save(util);
            URI location = linkTo(UtilisateurRepresentation.class).slash(saved.getId()).toUri();
            return ResponseEntity.created(location).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Un utilisateur possède déjà l'email que vous utilisez.");
        }
    }
    
    // PUT
    @PutMapping(value = "/{utilisateurId}")
    @Transactional
    public ResponseEntity<?> updateUtilisateur(@RequestBody Utilisateur utilisateur,
              @PathVariable("utilisateurId") String utilisateurId) {
                    return ures.findById(utilisateurId)
                    .map(util -> {
                            utilisateur.setId(utilisateurId);
                            utilisateur.setMotDePasse( CustomWebSecurityConfigurerAdapter.passwordEncoder().encode(utilisateur.getMotDePasse()));
                            ures.save(utilisateur);
                            return ResponseEntity.ok().build();
                    })
                    .orElseGet(() -> {
                        if (ures.findByMail(utilisateur.getMail()) == null){
                            Utilisateur utilisateurPut = new Utilisateur(UUID.randomUUID().toString(), utilisateur.getMail(), CustomWebSecurityConfigurerAdapter.passwordEncoder().encode(utilisateur.getMotDePasse()) ,utilisateur.getNom(), utilisateur.getPrenom(), null);
                            Utilisateur saved = ures.save(utilisateurPut);
                            URI location = linkTo(UtilisateurRepresentation.class).slash(saved.getId()).toUri();
                            return ResponseEntity.created(location).build();
                        }else{
                            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Un utilisateur possède déjà l'email que vous utilisez.");  
                        }
                        
                    });
      }


    private CollectionModel<EntityModel<Utilisateur>> utilisateurToResource(Iterable<Utilisateur> utilisateurs) {
        Link selfLink = linkTo(methodOn(UtilisateurRepresentation.class).getAllUtilisateurs()).withSelfRel();
        var postUtilisateurs = linkTo(UtilisateurRepresentation.class).withRel("POST - Pour ajouter un utilisateur");
        List<EntityModel<Utilisateur>> utilisateurResources = new ArrayList();
        utilisateurs.forEach(utilisateur -> utilisateurResources.add(utilisateurToResource(utilisateur, false)));
        return CollectionModel.of(utilisateurResources, selfLink, postUtilisateurs);
    }

    private EntityModel<Utilisateur> utilisateurToResource(Utilisateur utilisateur, Boolean collection) {
        var selfLink = linkTo(UtilisateurRepresentation.class).slash(utilisateur.getId()).withSelfRel();
        var selfLink2 = linkTo(UtilisateurRepresentation.class).slash(utilisateur.getId()).withRel("POST - PUT - DELETE possible");
        if (Boolean.TRUE.equals(collection)) {
            Link collectionLink = linkTo(methodOn(UtilisateurRepresentation.class).getAllUtilisateurs())
                    .withRel("collection");
            return EntityModel.of(utilisateur, selfLink, selfLink2, collectionLink);
        } else {
            return EntityModel.of(utilisateur, selfLink, selfLink2);
        }
    }
}
