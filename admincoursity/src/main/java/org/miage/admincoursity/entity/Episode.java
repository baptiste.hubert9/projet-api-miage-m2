package org.miage.admincoursity.entity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.FetchType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;


import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.RequiredArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Episode extends RepresentationModel<Episode> implements Serializable   {

    private static final long serialVersionUID = 8765432234567L;

    @Id
    @NonNull
    private String id;

    @NonNull
    private String idCours;

    @NonNull
    @NotBlank(message = "Le nom de l'épisode est obligatoire")
    @Size(min=3)
    private String nom;

    @NonNull
    @NotBlank(message = "La source de l'épisode est obligatoire")
    @Size(min=3)
    private String source;

    @NonNull
    @Min(1)
    @Max(180)
    private int tempsEnMinutes;
    
    @JsonIgnore
    @Transient
    @ManyToOne(targetEntity = Cours.class, fetch=FetchType.LAZY)
    private Cours cours;
    

    
}