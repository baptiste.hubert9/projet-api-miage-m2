package org.miage.admincoursity.entity;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

//Cette classe ne sert qu'au moment du post de l'utilisateur pour ajouter un cours. Il saisie les infos du cours dans le body, puis fournit une liste d'épisodes.
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoursEpisodes {

    private Cours cours;
    private List<Episode> listeEpisode;
   
}