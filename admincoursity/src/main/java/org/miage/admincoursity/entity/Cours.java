package org.miage.admincoursity.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.NonNull;


@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Cours implements Serializable {

    private static final long serialVersionUID = 8765432234567L;
    @Id
    @NonNull
    private String id;

    @NonNull
    @NotBlank(message = "Le theme est obligatoire")
    @Size(min=2)
    private String theme;

    @NonNull
    @NotNull
    @Min(1)
    @Max(500)
    private int PrixEnEuros;
    
    @Transient
    private List<Episode> listeEpisode;
}