package org.miage.admincoursity.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



@EnableWebSecurity
@Configuration
public class CustomWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Autowired 
    public CustomWebSecurityConfigurerAdapter() {
    }

    public static String alphaNumericString(int len) {
      String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      Random rnd = new Random();
      StringBuilder sb = new StringBuilder(len);
      for (int i = 0; i < len; i++) {
          sb.append(AB.charAt(rnd.nextInt(AB.length())));
      }
      return sb.toString();
  }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
      String valeurMdp = alphaNumericString(10);
      String randomPass = passwordEncoder().encode(valeurMdp);
      System.out.println("*********************************- password auto-généré : " + valeurMdp + " -*********************************");
      auth.inMemoryAuthentication().withUser("admin").password(randomPass).roles("ADMIN");
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
    }

protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().formLogin().disable()
        .authorizeRequests()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .defaultSuccessUrl("/", true);
    http.httpBasic();
                
  }
}