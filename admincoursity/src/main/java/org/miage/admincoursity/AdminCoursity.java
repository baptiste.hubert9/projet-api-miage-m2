package org.miage.admincoursity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@SpringBootApplication
public class AdminCoursity {

    public static void main(String[] args) {
        SpringApplication.run(AdminCoursity.class, args);
    }

    @Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI().info(new Info()
		.title("AdminCoursity API")
		.version("1.0")
		.description("Documentation de l'administration de Coursity API Miage"));
	}
}